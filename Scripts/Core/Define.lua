Global('Define', function(name, construct)
	assert(name);
	assert(construct);

	Global(name, function(...)
		local module = {};
		local environment = {this = module};

		setmetatable(environment, {__index = _G, __newindex = module});
		
		setfenv(construct, environment);
		
		construct(...);
		
		return module;
	end);
end);