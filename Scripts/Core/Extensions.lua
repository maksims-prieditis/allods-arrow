Global('passed', function(...)
	return select('#', ...) > 0;
end);

rawset(table, 'each', function(x, callback)
	assert(type(x) == 'table');
	
	for key, value in pairs(x) do
		if callback(value, key) then
			return;
		end;
	end;
end);
