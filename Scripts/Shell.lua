local _hooks = Hooks();
local _arrow;

function positionOf(id)
	if id and object.IsExist(id) and id ~= avatar.GetId() then
		return object.GetPos(id);
	end;
	
	return nil;
end;

function onStateChanged(e)
	local position = positionOf(avatar.GetTarget());
	
	if position then
		local me = avatar.GetPos();
		local sideX = me.posX - position.posX;
		local sideY = me.posY - position.posY;
		
		local distance = math.sqrt(sideX * sideX + sideY * sideY);
		
		if distance <= 30 then
			_arrow:SetBackgroundColor({r = 0; g = .86; b = 0; a = 1});
		elseif distance <= 50 then
			_arrow:SetBackgroundColor({r = 1; g = .86; b = 0; a = 1});
		else
			_arrow:SetBackgroundColor({r = .9; g = 0; b = 0; a = 1});
		end;
		
		local direction = math.atan2(sideY, sideX);
		
		_arrow:Rotate(direction - mission.GetCameraDirection());
		_arrow:Show(true);
	else
		_arrow:Show(false);
	end;
end;

function onInitialize()
	_hooks.unbind('EVENT_AVATAR_CREATED');
	
	_arrow = mainForm:GetChildChecked('Arrow', false);
	
	onStateChanged();
	
	_hooks.bind('EVENT_AVATAR_POS_CHANGED', onStateChanged);
	_hooks.bind('EVENT_CAMERA_DIRECTION_CHANGED', onStateChanged);
	_hooks.bind('EVENT_AVATAR_TARGET_CHANGED', onStateChanged);
end;
	
if avatar.IsExist() then
	onInitialize();
else
	_hooks.bind('EVENT_AVATAR_CREATED', onInitialize);
end
	
_hooks.enabled(true);
